+++
title = "About"
date = "2020-10-10"
aliases = ["contact"]
+++

Welcome! I'm using this site to write about stuff I find interesting and/or frustrating enough to write a blog post about. Opinions that I express here are my own, and not those of my current employer (TU Darmstadt). [I'm](https://www.mma.tu-darmstadt.de/index/mitarbeiter_3/mitarbeiter_details_mma_43648.en.jsp) an [Athene young Investigator](https://www.tu-darmstadt.de/forschen/wissenschaftlicher_nachwuchs_tu/qualifikationsphase_fuer_eine_professur/athene_young_investigator/index.en.jsp) at [TU Darmstadt](https://www.tu-darmstadt.de/index.en.jsp) and I'm working at the [Mathematical Modeling and Analysis Institute](https://www.mma.tu-darmstadt.de/index/index.en.jsp) lead by [Professor Dieter Bothe](https://www.mma.tu-darmstadt.de/index/mitarbeiter_3/mitarbeiter_details_mma_43073.en.jsp). 

I'm working in the field of Computational Fluid Dynamics, specifically on multiphase flows, more specifically on approximating moving surfaces that separate fluids that do not mix with each other, and I do this in [OpenFOAM](https://openfoam.com/). 
